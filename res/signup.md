# Cadastro

>##  Caso de Sucesso

- ✔️ Valida recebimento dos campos nome, email, senha
- ✔️ Valida senha comparando com confirmação de senha
- ✔️ Valida email
- ❌ Confirma se email existe
- ❌ Oferta 2FA
- ✔️ Retorna token de acesso

>##  Exceções

- ✔️ Retorna 400 se o(s) campo(s) nome, email, senha ou confirmação da senha não for(em) fornecido(s)
- ✔️ Retorna 400 se a senha e a confirmação forem diferentes
- ✔️ Retorna 400 se email for inválido 
- ✔️ Retorna 403 se o email fornecido estiver em uso
- ✔️ Retorna 500 caso não consiga salvar a conta criada
- ✔️ Retorna 500 caso não consiga autenticar
