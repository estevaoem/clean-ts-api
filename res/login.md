# Login

>##  Caso de Sucesso

- ✔️ Valida recebimento dos campos email e senha
- ✔️ Valida email
- ❌ Recebe 2FA
- ✔️ Retorna 200 com token de acesso

>##  Exceções

- ✔️ Retorna 400 se o(s) campo(s) email ou senha não for(em) fornecido(s)
- ✔️ Retorna 400 se email for inválido 
- ✔️ Retorna 401 se as credenciais forem inválidas 
- ✔️ Retorna 500 em caso de erro de autenticação
