import { AuthenticatorModel } from '../models/authenticator'

export interface Authenticator {
  auth: (authentication: AuthenticatorModel) => Promise<string | null>
}
