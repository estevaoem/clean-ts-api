import { AddAccountModel } from '.'
import { AccountModel } from '../models/account'

export interface AddAccount {
  add: (account: AddAccountModel) => Promise<AccountModel | null>
}
