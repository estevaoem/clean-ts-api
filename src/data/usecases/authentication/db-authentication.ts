import { AuthenticatorModel } from '../../../domain/models/authenticator'
import { Authenticator } from '../../../domain/usecases'
import { HashComparer } from '../../protocols/cryptography/hash-comparer'
import { Encrypter } from '../../protocols/cryptography/encrypter'
import { LoadAccountByEmailRepository } from '../../protocols/db/account/load-account-by-email-repository'
import { UpdateAccessTokenRepository } from '../../protocols/db/account/update-access-token-repository'

export class DbAuthenticator implements Authenticator {
  constructor (
    private readonly loadAccountByEmailRepository: LoadAccountByEmailRepository,
    private readonly hashComparer: HashComparer,
    private readonly encrypter: Encrypter,
    private readonly updateAccessTokenRepository: UpdateAccessTokenRepository
  ) {}

  async auth (authentication: AuthenticatorModel): Promise<string | null> {
    const account = await this.loadAccountByEmailRepository.loadAccountByEmail(
      authentication.email
    )
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (!account) {
      return null
    }

    const equalValues = await this.hashComparer.compare(
      authentication.password,
      account.password
    )
    if (!equalValues) {
      return null
    }

    const accessToken = await this.encrypter.encrypt(account.id)
    await this.updateAccessTokenRepository.updateAccessToken(
      account.id,
      accessToken
    )
    return accessToken
  }
}
