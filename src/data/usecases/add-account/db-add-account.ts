import { AccountModel } from '../../../domain/models/account'
import { AddAccount, AddAccountModel } from '../../../domain/usecases'
import { AddAccountRepository } from '../../protocols/db/account/add-account-repository'
import { Hasher } from '../../protocols/cryptography/hasher'
import { LoadAccountByEmailRepository } from '../../protocols/db/account/load-account-by-email-repository'

export class DbAddAccount implements AddAccount {
  constructor (
    private readonly loadAccountByEmailRepository: LoadAccountByEmailRepository,
    private readonly hasher: Hasher,
    private readonly addAccountRepository: AddAccountRepository
  ) {}

  async add (accountData: AddAccountModel): Promise<AccountModel | null> {
    const { email, password } = accountData
    const accountOnDb = await this.loadAccountByEmailRepository.loadAccountByEmail(email)

    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (accountOnDb) {
      return null
    }

    const hashedPassword = await this.hasher.hash(password)
    const securedAccount = Object.assign({}, accountData, {
      password: hashedPassword
    })
    const account = await this.addAccountRepository.add(securedAccount)
    return await new Promise((resolve) => resolve(account))
  }
}
