import { AccountModel } from '../../../../domain/models/account'

export interface LoadAccountByIdRepository {
  loadAccountById: (id: string) => Promise<AccountModel | null>
}
