import request from 'supertest'
import { AddAccountModel } from '../../../domain/usecases'
import { MongoHelper } from '../../../infra/db/mongodb/helpers/mongo-helper'
import app from '../../config/app'

const makeAccountData = (): AddAccountModel => ({
  name: 'loginRoutes_name',
  email: 'loginRoutes_email@mail.com',
  password: 'loginRoutes_password'
})

describe('Login Routes', () => {
  beforeAll(async () => {
    await MongoHelper.connect(process.env.MONGO_URL as string)
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  beforeEach(async () => {
    const accountCollection = await MongoHelper.getCollection('accounts')
    await accountCollection.deleteMany({})
  })

  describe('POST /login', () => {
    test('Should return 200 upon login', async () => {
      const accountData = makeAccountData()
      await request(app).post('/api/signup').send({
        name: accountData.name,
        email: accountData.email,
        password: accountData.password,
        passwordConfirmation: accountData.password
      })

      await request(app)
        .post('/api/login')
        .send({
          email: accountData.email,
          password: accountData.password
        })
        .expect(200)
    })

    test('Should return 401 if login fails', async () => {
      const accountData = makeAccountData()

      await request(app)
        .post('/api/login')
        .send({
          email: accountData.email,
          password: accountData.password
        })
        .expect(401)
    })
  })
})
