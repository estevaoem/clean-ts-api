import { MongoHelper } from '../infra/db/mongodb/helpers/mongo-helper'
import env from './config/env'

console.log('Connecting to mongodb...')

MongoHelper.connect(env.mongoUrl)
  .then(async () => {
    console.log(`Db connected at ${env.mongoUrl}`)
    console.log('Starting server...')
    const app = (await import('./config/app')).default
    app.listen(env.port, () =>
      console.log(`Server running at http://127.0.0.1:${env.port}`)
    )
  })
  .catch(console.error)
