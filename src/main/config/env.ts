export default {
  mongoUrl: process.env.MONGO_URL ?? 'mongodb://mongo:27017/clean-node-api',
  port: process.env.PORT ?? 5050,
  salt: process.env.SALT ?? 12,
  jwtSecret:
    process.env.JWT_SECRET ??
    '/O%Y6>Zw$F=Xmbsn%LU4b_v7?G^e2|q[k=%*X[&Q]*Xp0Ty_63G+"?NVm=>{W.T}'
}
