import { Express, Router } from 'express'
import { readdirSync } from 'fs'
import path from 'path'

export default (app: Express): void => {
  const router = Router()
  app.use('/api', router)

  const routesDirPath = path.join(__dirname, '../routes')
  readdirSync(routesDirPath).map(async (directory) => {
    const filePath = path.join(__dirname, '../routes', directory)
    readdirSync(filePath).map(async (file) => {
      if (!file.includes('.test.')) {
        const route = (await import(`../routes/${directory}/${file}`)).default
        route(router)
      }
    })
  })
}
