import { MongoLogRepository } from '../../../infra/db/mongodb/log/mongo-log-repository'
import { Controller } from '../../../presentation/protocols'
import { ControllerLogDecorator } from '../../decorators/controller-log-decorator'

export const makeControllerLogDecorator = (controller: Controller): Controller => {
  const mongoLogRepository = new MongoLogRepository()
  return new ControllerLogDecorator(controller, mongoLogRepository)
}
