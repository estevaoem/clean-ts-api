import { EmailValidator } from '../../../../validator/protocols/email-validator'
import {
  CompareFieldsValidation,
  EmailValidation,
  RequiredFieldsValidation,
  ValidationComposite
} from '../../../../validator/validation'
import { makeSignupValidationComposite } from './signup-validation-factory'

const makeEmailValidator = (): EmailValidator => {
  class EmailValidatorStub implements EmailValidator {
    isValid (email: string): boolean {
      return true
    }
  }
  return new EmailValidatorStub()
}

jest.mock('../../../../validator/validation/validation-composite')

describe('Signup Validation Composite Factory', () => {
  test('Should call SignupValidation Composite with all values', async () => {
    const sut = makeSignupValidationComposite
    sut()
    expect(ValidationComposite).toHaveBeenCalledWith([
      new RequiredFieldsValidation([
        'name',
        'email',
        'password',
        'passwordConfirmation'
      ]),
      new CompareFieldsValidation('password', 'passwordConfirmation'),
      new EmailValidation('email', makeEmailValidator())
    ])
  })
})
