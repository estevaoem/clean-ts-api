import {
  CompareFieldsValidation,
  EmailValidation,
  RequiredFieldsValidation,
  ValidationComposite
} from '../../../../validator/validation'
import { EmailValidatorAdapter } from '../../../../infra/validator/email-validator-adapter'

export const makeSignupValidationComposite = (): ValidationComposite => {
  return new ValidationComposite([
    new RequiredFieldsValidation([
      'name',
      'email',
      'password',
      'passwordConfirmation'
    ]),
    new CompareFieldsValidation('password', 'passwordConfirmation'),
    new EmailValidation('email', new EmailValidatorAdapter())
  ])
}
