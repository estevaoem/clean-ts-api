import { SignupController } from '../../../../presentation/controllers/signup/signup-controller'
import { Controller } from '../../../../presentation/protocols'
import { makeControllerLogDecorator } from '../../decorators/controller-log-decorator-factory'
import { makeDbAddAccount } from '../../usecases/add-account/db-add-account-factory'
import { makeDbAuthenticator } from '../../usecases/authenticator/db-authenticator-factory'
import { makeSignupValidationComposite } from './signup-validation-factory'

export const makeSignupController = (): Controller => {
  const dbAddAccount = makeDbAddAccount()
  const validation = makeSignupValidationComposite()
  const dbAuthenticator = makeDbAuthenticator()
  const signupController = new SignupController(
    dbAddAccount,
    validation,
    dbAuthenticator
  )
  return makeControllerLogDecorator(signupController)
}
