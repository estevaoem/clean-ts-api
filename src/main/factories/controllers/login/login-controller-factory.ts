import { LoginController } from '../../../../presentation/controllers/login/login-controller'
import { Controller } from '../../../../presentation/protocols'
import { makeControllerLogDecorator } from '../../decorators/controller-log-decorator-factory'
import { makeDbAuthenticator } from '../../usecases/authenticator/db-authenticator-factory'
import { makeLoginValidationComposite } from './login-validation-factory'

export const makeLoginController = (): Controller => {
  const validation = makeLoginValidationComposite()
  const dbAuthenticator = makeDbAuthenticator()
  const loginController = new LoginController(validation, dbAuthenticator)
  return makeControllerLogDecorator(loginController)
}
