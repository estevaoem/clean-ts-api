import { EmailValidator } from '../../../../validator/protocols/email-validator'
import {
  EmailValidation,
  RequiredFieldsValidation,
  ValidationComposite
} from '../../../../validator/validation'
import { makeLoginValidationComposite } from './login-validation-factory'

const makeEmailValidator = (): EmailValidator => {
  class EmailValidatorStub implements EmailValidator {
    isValid (email: string): boolean {
      return true
    }
  }
  return new EmailValidatorStub()
}

jest.mock('../../../../validator/validation/validation-composite')

describe('Login Validation Composite Factory', () => {
  test('Should call LoginValidation Composite with all values', async () => {
    const sut = makeLoginValidationComposite
    sut()
    expect(ValidationComposite).toHaveBeenCalledWith([
      new RequiredFieldsValidation(['email', 'password']),
      new EmailValidation('email', makeEmailValidator())
    ])
  })
})
