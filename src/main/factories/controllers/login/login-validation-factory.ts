import {
  EmailValidation,
  RequiredFieldsValidation,
  ValidationComposite
} from '../../../../validator/validation'
import { EmailValidatorAdapter } from '../../../../infra/validator/email-validator-adapter'

export const makeLoginValidationComposite = (): ValidationComposite => {
  return new ValidationComposite([
    new RequiredFieldsValidation(['email', 'password']),
    new EmailValidation('email', new EmailValidatorAdapter())
  ])
}
