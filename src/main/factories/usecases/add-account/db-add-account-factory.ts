import { DbAddAccount } from '../../../../data/usecases/add-account/db-add-account'
import { AddAccount } from '../../../../domain/usecases'
import { BcryptAdapter } from '../../../../infra/criptography/bcrypt-adapter/bcrypt-adapter'
import { MongoAccountRepository } from '../../../../infra/db/mongodb/account/mongo-account-repository'
import env from '../../../config/env'

export const makeDbAddAccount = (): AddAccount => {
  const loadAccountByEmail = new MongoAccountRepository()
  const bcryptAdapter = new BcryptAdapter(env.salt as number)
  const mongoAccountRepository = loadAccountByEmail
  return new DbAddAccount(loadAccountByEmail, bcryptAdapter, mongoAccountRepository)
}
