import { DbAuthenticator } from '../../../../data/usecases/authentication/db-authentication'
import { Authenticator } from '../../../../domain/usecases'
import { BcryptAdapter } from '../../../../infra/criptography/bcrypt-adapter/bcrypt-adapter'
import { JwtAdapter } from '../../../../infra/criptography/jwt-adapter/jwt-adapter'
import { MongoAccountRepository } from '../../../../infra/db/mongodb/account/mongo-account-repository'
import env from '../../../config/env'

export const makeDbAuthenticator = (): Authenticator => {
  const loadAccountByEmailRepository = new MongoAccountRepository()
  const hashComparer = new BcryptAdapter(env.salt as number)
  const encrypter = new JwtAdapter(env.jwtSecret)
  const updateAccessTokenRepository = loadAccountByEmailRepository
  return new DbAuthenticator(
    loadAccountByEmailRepository,
    hashComparer,
    encrypter,
    updateAccessTokenRepository
  )
}
