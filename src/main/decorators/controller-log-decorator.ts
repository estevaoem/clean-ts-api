import { LogErrorRepository } from '../../data/protocols/db/log/error-log-repository'
import {
  Controller,
  HttpRequest,
  HttpResponse
} from '../../presentation/protocols'

export class ControllerLogDecorator implements Controller {
  constructor (
    private readonly controller: Controller,
    private readonly errorLogRepository: LogErrorRepository
  ) {}

  async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
    const httpResponse = await this.controller.handle(httpRequest)
    if (httpResponse.statusCode === 500) {
      await this.errorLogRepository.logError(httpResponse.body.stack)
    }
    return httpResponse
  }
}
