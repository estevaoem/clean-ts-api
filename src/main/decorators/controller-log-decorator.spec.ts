import { LogErrorRepository } from '../../data/protocols/db/log/error-log-repository'
import { serverError } from '../../presentation/helpers/http/http-helper'
import {
  Controller,
  HttpRequest,
  HttpResponse
} from '../../presentation/protocols'
import { ControllerLogDecorator } from './controller-log-decorator'

interface SutTypes {
  controllerStub: Controller
  sut: ControllerLogDecorator
  errorLogRepositoryStub: LogErrorRepository
}

const makeErrorLogRepositoryStub = (): LogErrorRepository => {
  class ErrorLogRepositoryStub implements LogErrorRepository {
    async logError (stack: string): Promise<void> {
      return await new Promise((resolve) => resolve())
    }
  }
  return new ErrorLogRepositoryStub()
}

const makeSut = (): SutTypes => {
  class ControllerStub implements Controller {
    async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
      const httpResponse = {
        statusCode: 200,
        body: {}
      }
      return await new Promise((resolve) => resolve(httpResponse))
    }
  }
  const controllerStub = new ControllerStub()
  const errorLogRepositoryStub = makeErrorLogRepositoryStub()
  const sut = new ControllerLogDecorator(controllerStub, errorLogRepositoryStub)
  return {
    controllerStub,
    errorLogRepositoryStub,
    sut
  }
}

const makeHttpRequest = (): HttpRequest => ({
  body: {
    name: 'any_name',
    email: 'any_email@mail.com',
    password: 'any_password',
    passwordConfirmation: 'any_password'
  }
})

describe('ControllerLogDecorator', () => {
  test('Should call received controllers handle method', async () => {
    const { sut, controllerStub } = makeSut()
    const handleSpy = jest.spyOn(controllerStub, 'handle')
    const httpRequest = makeHttpRequest()
    await sut.handle(httpRequest)
    expect(handleSpy).toHaveBeenCalledWith(httpRequest)
  })

  test('Should return the same result of received controller', async () => {
    const { sut, controllerStub } = makeSut()
    const httpRequest = makeHttpRequest()
    const controllerStubHttpResponse = await controllerStub.handle(httpRequest)
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse).toEqual(controllerStubHttpResponse)
  })

  test('Should call ErrorLogRepository with correct error if controller returns a server error', async () => {
    const { sut, controllerStub, errorLogRepositoryStub } = makeSut()
    const error = new Error()
    error.stack = 'error_stack'
    const logSpy = jest.spyOn(errorLogRepositoryStub, 'logError')
    jest
      .spyOn(controllerStub, 'handle')
      .mockReturnValueOnce(
        new Promise((resolve) => resolve(serverError(error)))
      )
    const httpRequest = makeHttpRequest()
    await sut.handle(httpRequest)
    expect(logSpy).toHaveBeenCalledWith('error_stack')
  })
})
