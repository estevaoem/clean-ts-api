import { InvalidParamError } from '../../presentation/errors'
import { HttpRequest } from '../../presentation/protocols'
import { CompareFieldsValidation } from './compare-fields-validation'

const makeHttpRequest = (): HttpRequest => ({
  body: {
    name: 'any_name',
    email: 'any_email@mail.com',
    password: 'any_password',
    passwordConfirmation: 'any_password'
  }
})

describe('CompareFieldsValidation', () => {
  test('Should return an InvalidParamError if fields differ', async () => {
    const sut = new CompareFieldsValidation('password', 'passwordConfirmation')
    const httpRequest = makeHttpRequest()
    httpRequest.body.passwordConfirmation = 'different_password'
    const validation = sut.validate(httpRequest.body)
    expect(validation).toEqual(new InvalidParamError('passwordConfirmation'))
  })

  test('Should return null on success', () => {
    const sut = new CompareFieldsValidation('password', 'passwordConfirmation')
    const httpRequest = makeHttpRequest()
    const validation = sut.validate(httpRequest.body)
    expect(validation).toBe(null)
  })
})
