import { Validation } from '../../presentation/protocols/validation'

export class ValidationComposite implements Validation {
  constructor (private readonly validationData: Validation[]) {}

  validate (input: any): Error | null {
    for (const validation of this.validationData) {
      const error = validation.validate(input)
      // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
      if (error) {
        return error
      }
    }
    return null
  }
}
