import { MissingParamError } from '../../presentation/errors'
import { Validation } from '../../presentation/protocols/validation'

export class RequiredFieldsValidation implements Validation {
  constructor (private readonly fields: string[]) {}

  validate (input: any): Error | null {
    for (const field of this.fields) {
      // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
      if (!input[field]) {
        return new MissingParamError(field)
      }
    }
    return null
  }
}
