import { HttpRequest } from '../../presentation/protocols'
import { MissingParamError } from '../../presentation/errors'
import { RequiredFieldsValidation } from './required-fields-validation'

const makeHttpRequest = (): HttpRequest => ({
  body: {
    name: 'any_name',
    email: 'any_email@mail.com',
    password: 'any_password',
    passwordConfirmation: 'any_password'
  }
})

const makeMissingHttpRequest = (missingParam: string): HttpRequest => {
  const completeHttpRequest = makeHttpRequest()
  const missingHttpRequest = {
    body: {}
  }
  for (const requirement in completeHttpRequest.body) {
    if (requirement !== missingParam) {
      missingHttpRequest.body[requirement] =
        completeHttpRequest.body[requirement]
    }
  }
  return missingHttpRequest
}

describe('RequiredFieldsValidation', () => {
  test('Should return a MissingParamError if a required field is missing', () => {
    const sut = new RequiredFieldsValidation([
      'name',
      'email',
      'password',
      'passwordConfirmation'
    ])
    const httpRequest = makeMissingHttpRequest('email')
    const validation = sut.validate(httpRequest.body)
    expect(validation).toEqual(new MissingParamError('email'))
  })

  test('Should return null on success', () => {
    const sut = new RequiredFieldsValidation([
      'name',
      'email',
      'password',
      'passwordConfirmation'
    ])
    const httpRequest = makeHttpRequest()
    const validation = sut.validate(httpRequest.body)
    expect(validation).toBe(null)
  })
})
