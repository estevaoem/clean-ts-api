import { InvalidParamError } from '../../presentation/errors'
import { EmailValidator } from '../protocols/email-validator'
import { EmailValidation } from './email-validation'

const makeEmailValidator = (): EmailValidator => {
  class EmailValidatorStub implements EmailValidator {
    isValid (email: string): boolean {
      return true
    }
  }
  return new EmailValidatorStub()
}

interface SutTypes {
  sut: EmailValidation
  emailValidatorStub: EmailValidator
}

const makeSut = (fieldName: string): SutTypes => {
  const emailValidatorStub = makeEmailValidator()
  const sut = new EmailValidation(fieldName, emailValidatorStub)
  return {
    sut,
    emailValidatorStub
  }
}

const toThrowAnError = (): any => {
  throw new Error()
}

describe('EmailValidation', () => {
  test('Should call EmailValidator with correct value', () => {
    const { sut, emailValidatorStub } = makeSut('email')
    const isValidSpy = jest.spyOn(emailValidatorStub, 'isValid')
    sut.validate({ email: 'valid_email@mail.com' })
    expect(isValidSpy).toHaveBeenCalledWith('valid_email@mail.com')
  })

  test('Should return an error if an invalid email is provided', async () => {
    const { sut, emailValidatorStub } = makeSut('email')
    jest.spyOn(emailValidatorStub, 'isValid').mockReturnValueOnce(false)
    const validation = sut.validate({ email: 'invalid_email@mail.com' })
    expect(validation).toEqual(new InvalidParamError('email'))
  })

  test('Should throw if EmailValidator throws', async () => {
    const { sut, emailValidatorStub } = makeSut('email')
    jest
      .spyOn(emailValidatorStub, 'isValid')
      .mockImplementationOnce(toThrowAnError)
    expect(sut.validate).toThrow()
  })
})
