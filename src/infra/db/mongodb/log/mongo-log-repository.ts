import { LogErrorRepository } from '../../../../data/protocols/db/log/error-log-repository'
import { MongoHelper } from '../helpers/mongo-helper'

export class MongoLogRepository implements LogErrorRepository {
  async logError (stack: string): Promise<void> {
    const errorCollection = await MongoHelper.getCollection('errors')
    await errorCollection.insertOne({
      stack,
      date: new Date()
    })
  }
}
