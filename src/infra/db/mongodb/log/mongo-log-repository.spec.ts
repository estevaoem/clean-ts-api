import { Collection } from 'mongodb'
import { MongoHelper } from '../helpers/mongo-helper'
import { MongoLogRepository } from './mongo-log-repository'

describe('Mongo Log Repository', () => {
  let errorCollection: Collection

  beforeAll(async () => {
    await MongoHelper.connect(process.env.MONGO_URL as string)
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  beforeEach(async () => {
    errorCollection = await MongoHelper.getCollection('errors')
    await errorCollection.deleteMany({})
  })

  test('Should create an error log on success', async () => {
    const sut = new MongoLogRepository()
    await sut.logError('any_error')
    const documentsCount = await errorCollection.countDocuments()
    expect(documentsCount).toBe(1)
  })
})
