import { ObjectId } from 'mongodb'
import { AddAccountRepository } from '../../../../data/protocols/db/account/add-account-repository'
import { LoadAccountByEmailRepository } from '../../../../data/protocols/db/account/load-account-by-email-repository'
import { LoadAccountByIdRepository } from '../../../../data/protocols/db/account/load-account-by-id-repository'
import { UpdateAccessTokenRepository } from '../../../../data/protocols/db/account/update-access-token-repository'
import { AccountModel } from '../../../../domain/models/account'
import { AddAccountModel } from '../../../../domain/usecases'
import { MongoHelper } from '../helpers/mongo-helper'

export class MongoAccountRepository
implements
    AddAccountRepository,
    LoadAccountByEmailRepository,
    LoadAccountByIdRepository,
    UpdateAccessTokenRepository {
  async add (accountData: AddAccountModel): Promise<AccountModel> {
    const accountCollection = await MongoHelper.getCollection('accounts')
    const insertOneResult = await accountCollection.insertOne(accountData)
    const findOneResult = await accountCollection.findOne({
      _id: insertOneResult.insertedId
    })
    const account = MongoHelper.getDataFromQuery(findOneResult)
    return account as AccountModel
  }

  async loadAccountByEmail (email: string): Promise<AccountModel | null> {
    const accountCollection = await MongoHelper.getCollection('accounts')
    const loadResult = await accountCollection.findOne({ email })
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (!loadResult) {
      return null
    }

    const account = MongoHelper.getDataFromQuery(loadResult)
    return account as AccountModel
  }

  async loadAccountById (id: string): Promise<AccountModel | null> {
    const accountCollection = await MongoHelper.getCollection('accounts')
    const loadResult = await accountCollection.findOne({
      _id: new ObjectId(id)
    })
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (!loadResult) {
      return null
    }

    const account = MongoHelper.getDataFromQuery(loadResult)
    return account as AccountModel
  }

  async updateAccessToken (id: string, token: string): Promise<void> {
    const accountCollection = await MongoHelper.getCollection('accounts')
    await accountCollection.updateOne(
      { _id: new ObjectId(id) },
      {
        $set: {
          accessToken: token
        }
      }
    )
  }
}
