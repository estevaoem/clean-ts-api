import { AddAccountModel } from '../../../../domain/usecases'
import { MongoHelper } from '../helpers/mongo-helper'
import { MongoAccountRepository } from './mongo-account-repository'

const makeSut = (): MongoAccountRepository => {
  return new MongoAccountRepository()
}

const makeAccountData = (): AddAccountModel => ({
  name: 'any_name',
  email: 'any_email@mail.com',
  password: 'any_password'
})

let accountCollection

describe('MongoAccountRepository', () => {
  beforeAll(async () => {
    await MongoHelper.connect(process.env.MONGO_URL as string)
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  beforeEach(async () => {
    accountCollection = await MongoHelper.getCollection('accounts')
    await accountCollection.deleteMany({})
  })

  test('Should add and return an account on success', async () => {
    const sut = makeSut()
    const accountData = makeAccountData()
    const account = await sut.add(accountData)
    expect(account).toBeTruthy()
    expect(account.id).toBeTruthy()
    expect(account.name).toBe('any_name')
    expect(account.email).toBe('any_email@mail.com')
    expect(account.password).toBe('any_password')
  })

  test('Should loadAccountByEmail and return an account on success', async () => {
    const sut = makeSut()
    await sut.add(makeAccountData())
    const account = await sut.loadAccountByEmail('any_email@mail.com')
    expect(account).toBeTruthy()
    expect(account?.id).toBeTruthy()
    expect(account?.name).toBe('any_name')
    expect(account?.email).toBe('any_email@mail.com')
    expect(account?.password).toBe('any_password')
  })

  test('Should loadAccountByEmail and return null on fail', async () => {
    const sut = makeSut()
    const account = await sut.loadAccountByEmail('any_email@mail.com')
    expect(account).toBe(null)
  })

  test('Should loadAccountById and return an account on success', async () => {
    const sut = makeSut()
    let account
    account = await sut.add(makeAccountData())
    account = await sut.loadAccountById(account?.id)
    expect(account).toBeTruthy()
    expect(account?.id).toBeTruthy()
    expect(account?.name).toBe('any_name')
    expect(account?.email).toBe('any_email@mail.com')
    expect(account?.password).toBe('any_password')
  })

  test('Should loadAccountById and return null on fail', async () => {
    const sut = makeSut()
    let account
    account = await sut.add(makeAccountData())
    accountCollection = await MongoHelper.getCollection('accounts')
    await accountCollection.deleteMany({})
    account = await sut.loadAccountById(account?.id)
    expect(account).toBe(null)
  })

  test('Should update account upon updateAccessToken success', async () => {
    const sut = makeSut()
    let account
    account = await sut.add(makeAccountData())
    await sut.updateAccessToken(account?.id, 'any_token')
    account = await sut.loadAccountById(account?.id)
    expect(account).toBeTruthy()
    expect(account?.accessToken).toBe('any_token')
  })
})
