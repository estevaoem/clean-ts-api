import bcrypt from 'bcrypt'
import env from '../../../main/config/env'
import { BcryptAdapter } from './bcrypt-adapter'

jest.mock('bcrypt', () => ({
  async hash (): Promise<string> {
    return await new Promise((resolve) => resolve('hash'))
  },

  async compare (): Promise<boolean> {
    return true
  }
}))

const makeSut = (): BcryptAdapter => {
  return new BcryptAdapter(env.salt as number)
}

const toThrowAnError = (): any => {
  throw new Error()
}

describe('BcryptAdapter', () => {
  test('Shoul call hasher with correct values', async () => {
    const sut = makeSut()
    const hashSpy = jest.spyOn(bcrypt, 'hash')
    await sut.hash('any_value')
    expect(hashSpy).toHaveBeenCalledWith('any_value', env.salt)
  })

  test('Should return a valid hash on hashing success', async () => {
    const sut = makeSut()
    const hash = await sut.hash('any_value')
    expect(hash).toBe('hash')
  })

  test('Should throw if hash throws', async () => {
    const sut = makeSut()
    jest.spyOn(bcrypt, 'hash').mockImplementationOnce(toThrowAnError)
    const hashPromise = sut.hash('any_value')
    void (await expect(hashPromise).rejects.toThrow())
  })

  test('Should call compare with correct values', async () => {
    const sut = makeSut()
    const compareSpy = jest.spyOn(bcrypt, 'compare')
    await sut.compare('any_value', 'any_hash')
    expect(compareSpy).toHaveBeenCalledWith('any_value', 'any_hash')
  })

  test('Should return true on compare success', async () => {
    const sut = makeSut()
    const compareResult = await sut.compare('any_value', 'any_hash')
    expect(compareResult).toBe(true)
  })

  test('Should return false on compare fail', async () => {
    const sut = makeSut()
    jest.spyOn(bcrypt, 'compare').mockImplementationOnce(() => false)
    const compareResult = await sut.compare('any_value', 'any_hash')
    expect(compareResult).toBe(false)
  })

  test('Should throw if compare throws', async () => {
    const sut = makeSut()
    jest.spyOn(bcrypt, 'compare').mockImplementationOnce(toThrowAnError)
    const comparePromise = sut.compare('any_value', 'any_hash')
    void (await expect(comparePromise).rejects.toThrow())
  })
})
