import { AddAccount, Authenticator } from '../../../domain/usecases'
import {
  badRequest,
  forbidden,
  ok,
  serverError
} from '../../helpers/http/http-helper'
import { Validation } from '../../protocols/validation'
import { Controller, HttpRequest, HttpResponse } from '../../protocols'
import { EmailInUseError } from '../../errors/email-in-use-error'

export class SignupController implements Controller {
  constructor (
    private readonly addAccount: AddAccount,
    private readonly validation: Validation,
    private readonly authenticator: Authenticator
  ) {}

  async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const error = this.validation.validate(httpRequest.body)

      // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
      if (error) {
        return badRequest(error)
      }

      const { name, email, password } = httpRequest.body

      const account = await this.addAccount.add({
        name,
        email,
        password
      })

      // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
      if (!account) {
        return forbidden(new EmailInUseError())
      }

      const accessToken = await this.authenticator.auth({ email, password })

      return ok({ accessToken })
    } catch (error) {
      return serverError(error)
    }
  }
}
