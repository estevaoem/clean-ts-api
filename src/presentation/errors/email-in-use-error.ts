export class EmailInUseError extends Error {
  constructor () {
    super('Received Email already in use')
    this.name = 'EmailInUseError'
  }
}
