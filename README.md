# Clean Typescript API

API developed for demonstrating knowledge in Typescript, node and its extensions.

The goal of this project is to create a standard repository that can be forked and at the same time allow my programming skills to be exposed to the job market.

>##  Developed APIs 🚀

-  [Signup](./res/signup.md)
-  [Login](./res/login.md)

>## Applied Principles ✏️

- Single Responsability Principle (SRP)
- Open Closed Principle (OCP)
- Liskov Substitution Principle (LSP)
- Interface Segregation Principle (ISP)
- Dependency Inversion Principle (DIP)
- Don't Repeat Yourself (DRY)
- You Aren't Gonna Need It (YAGNI)
- Keep It Simple, Silly (KISS)
- Small Commits

>## Methodologies and Designs 📋

- Test Driven Development (TDD)
- Clean Architecture
- Domain Driven Design (DDD)
- Conventional Commits
- Use Cases
- Continuous Deployment

>## Design Patterns 🧮

- Factories
- Adapters
- Composite
- Decorator
- Dependency Injection

>## Development Resources 🛠

- Typescript
- Node
- NPM
- Git
- Docker
- MongoDB

>## Development Libraries 📐

- Eslint
- Husky
- Jest
- Lint-Staged
- Nodemon
- Rimraf
- SuperTest


>## Production Libraries 🏭

- Bcrypt
- Express
- JsonWebToken
- In Memory Mongo
- Validator

>## Resources in Node 🟢

- Express REST API
- Error Logging
- Security (Hashing, Ecryption, Encoding)
- Access levels (admin, user, anon)
- CORS
- Middlewares

>## Resources in Testing ✔️

- Unit tests
- Integration tests
- Test coverage
- Mocks
- Stubs
- Spies

>## Resources in MongoDB 🍃

- Connect and Reconnect
- Collections
- FindOne
- Insert One
